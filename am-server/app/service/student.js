/*
 * @Description: 
 * @Author: charles
 * @Date: 2021-12-17 16:43:47
 * @LastEditors: charles
 * @LastEditTime: 2021-12-17 17:01:19
 */
const { Service } = require('egg')
const BussinessError = require('../utils/BussinessError')

class StudentService extends Service {

  async saveOrUpdate (row) {
    const { mysql } = this.app
    try {
      if (row.id) {
        // 更新 {id:1,name:"terry",no:2021110022}  update school_student set name='terry',no=2021 where id = 1
        await mysql.update('school_student', row)
      } else {
        await mysql.insert('school_student', row)
      }
    } catch (error) {
      console.log(error);
      throw new BussinessError('操作失败')
    }
  }



  async pageQuery ({ page, pageSize }) {
    const { mysql } = this.app;
    
    //1. 查询分页数据
    let sql_select = `select * from school_student limit ${(page - 1) * pageSize},${pageSize}`
    const students = await mysql.query(sql_select)

    //2. 统计数量
    const sql = `select count(*) as total from school_student`
    // 获取分页查询的数据条数
    const [{ total }] = await mysql.query(sql)
    // 返回
    return {
      page,
      pageSize,
      total,
      list: students
    }
  }
}
module.exports = StudentService;