/*
 * @Description: 
 * @Author: charles
 * @Date: 2021-12-16 09:39:13
 * @LastEditors: charles
 * @LastEditTime: 2021-12-20 14:56:56
 */
import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css
import '@/styles/element-variables.scss'
import '@/styles/customer.css'

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

import moment from 'moment'

Vue.filter('fmtDate', function (date) {
  return date ? moment(date).format('YYYY-MM-DD') : '';
})

Vue.filter('fmtTime', function (date) {
  return date ? moment(date).format('YYYY-MM-DD HH:mm:ss') : '';
})

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,

  store,
  render: h => h(App)
})
