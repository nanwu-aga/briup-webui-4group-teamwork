/*
 * @Description: 
 * @Author: charles
 * @Date: 2021-12-14 20:42:55
 * @LastEditors: charles
 * @LastEditTime: 2021-12-21 16:58:41
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
// 管理页面
import Manager from '../views/manager/Index'
import Home from '../views/manager/Home'
import Order from '../views/manager/Order'
import User from '../views/manager/User'
import Complete from '../views/manager/Complete'
// 登录页面
// 订单详情页
import OrderDetails from '../views/manager/OrderDetails.vue'
import Login from '../views/Login'
import { getToken } from '../utils/auth'
import { Toast } from 'vant'
import store from '../store'


Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    redirect: "/manager/home"
  },
  {
    path: '/manager',
    name: 'manager',
    component: Manager,
    beforeEnter: (to, from, next) => {  //属于路由自己的拦截器
      let token = getToken();
      if (token) {
        // 查询info
        store.dispatch('user/getInfo', token)
          .then(() => {
            // 当获取万用户信息之后才允许跳转
            next();
          })
      } else {
        Toast("token失效")
        // 跳转到登录
        next({ path: '/login' })
      }
    },
    children: [{
      path: 'home',
      component: Home,
    }, {
      path: 'order',
      component: Order,
    }, {
      path: 'user',
      component: User,
    }, {
      path: 'complete',
      component: Complete,
    }]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/orderDetails',
    name: 'orderDetails',
    component: OrderDetails
  },
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
